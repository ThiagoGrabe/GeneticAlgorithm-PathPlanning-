# GeneticAlgorithm-PathPlanning-
Genetic algorithm for holonomic robot path planning using Bezier curves

## Overview

In this repository I present a genetic algorithm for path planning based on Bezier Curves. The framework used to simulate is [ROS](http://www.ros.org/) - Robot Operating System in python.
